FROM python:alpine3.7
LABEL maintainer "luke@xors.com"
COPY . /app
WORKDIR /app
CMD python ./math.py
