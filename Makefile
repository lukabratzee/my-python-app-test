# dev:
# 	# creates docker network if not already made
# 	# docker network create --subnet=192.168.10.10/16 mynet123
# 	docker run -d -u 0 -it -p 8080:8080 -p 5000:5000 \
# 	-v jenkins_home:/var/jenkins_home \
# 	-v /var/run/docker.sock:/var/run/docker.sock \
# 	--restart unless-stopped \
# 	4oh4/jenkins-docker

docker_nginx:
	sudo docker run --name jenkins-nginx-container -v /etc/nginx/nginx.conf:/etc/nginx/nginx.conf:ro -d nginx
	
docker_jenkins:
	docker run -d -u 0 -it -p 8080:8080 -p 5000:5000 \
	-v jenkins_home:/var/jenkins_home \
	-v /var/run/docker.sock:/var/run/docker.sock \
	--restart unless-stopped \
	4oh4/jenkins-docker